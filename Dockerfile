FROM python:2.7

# Force stdin, stdout and stderr to be totally unbuffered.
ENV PYTHONUNBUFFERED 1

# Add PostgreSQL's repository.
RUN echo "deb http://apt.postgresql.org/pub/repos/apt/ precise-pgdg 9.6" > /etc/apt/sources.list.d/pgdg.list
RUN apt-get install wget ca-certificates
RUN wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | apt-key add -

# Install some tools not in the requirements
RUN apt-get update -y
RUN apt-get install -y postgresql-client

# Add requirements files
RUN mkdir /app
WORKDIR /app

# Install dev requirements
ADD requirements.txt /app/
RUN pip install -r requirements.txt

# Add the code
ADD . /app/

