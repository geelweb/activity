from django.shortcuts import render_to_response, redirect
from django.template import RequestContext
from django.http import HttpResponseRedirect
from django.conf import settings

def home(request):
    """
    Application home page
    """
    if request.user.is_authenticated():
        return redirect(settings.LOGGED_HOME_PAGE_VIEW)
    return render_to_response(
        'home.html', {}, context_instance=RequestContext(request))

