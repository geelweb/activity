from django.conf.urls import patterns, include, url
from django.conf import settings
from django.views.generic import TemplateView

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Home, about, contact...
    url(r'^$', settings.HOME_PAGE_VIEW, name='home'),
    url(r'^about/$', TemplateView.as_view(template_name='about.html'), name='about'),
    url(r'^contact/$', 'geelweb.django.contactform.views.contact', name='contact'),

    # core urls
    url(r'^', include('activity_core.urls')),

    # Statistics
    url(r'^report/', include('activity_report.urls')),
    url(r'^dashboard/', include('activity_dashboard.urls')),

    # Admin
    url(r'^admin/', include(admin.site.urls)),

    # Login / Logout
    url(r'^login/$', 'django.contrib.auth.views.login', name='login'),
    url(r'^logout/$', 'django.contrib.auth.views.logout', {'next_page':'/'}, name='logout'),
)
