# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('task_timer_api', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelTable(
            name='client',
            table='activity_core_client',
        ),
        migrations.AlterModelTable(
            name='job',
            table='activity_core_job',
        ),
        migrations.AlterModelTable(
            name='project',
            table='activity_core_project',
        ),
        migrations.AlterModelTable(
            name='task',
            table='activity_core_task',
        ),
    ]
