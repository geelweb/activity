var timer;
var Task = {
    toggle: function(id) {
        $.ajax({
            url: '/ajx/task/' + id + '/toggle/',
            success: function(responseJSON, responseText) {
                var start = stop = false;
                var seconds = 0;
                if (responseJSON.code != 200) {
                    console.error('Error toogling the state of the task ' + id);
                    return;
                }

                // statu = 101, the task has been started
                if (responseJSON.status == '101') {
                    $('a#start_stop_' + responseJSON.task_id).html('<span class="glyphicon glyphicon-pause"></span>');
                    start = true;
                    seconds = responseJSON.time;
                }

                // statu = 102, the task has been stoped
                if (responseJSON.status == '102') {
                    $('a#start_stop_' + responseJSON.task_id).html('<span class="glyphicon glyphicon-play"></span>');
                    stop = true;
                }

                $("#last-task").load("/ajx/task/last/", function() {
                    if (start) {
                        timer = setInterval(function() {
                            seconds = seconds + 1;

                            var h = m = s = 0;
                            h = Math.floor(seconds / 60 / 60);
                            m = Math.floor((seconds - h * 60) / 60);
                            s = Math.floor(seconds - m * 60 - h * 60 * 60);

                            var pad = '00';
                            m = '' + m;
                            m = pad.substring(0, pad.length - m.length) + m;
                            s = '' + s;
                            s = pad.substring(0, pad.length - s.length) + s;

                            $('#time').html(h + ':' + m + ':' + s);
                        }, 1000);
                    }
                    if (stop) {
                        clearInterval(timer);
                    }
                });
            }
        }).done();
    },

    setupButton: function(mode, task_id) {
        if (mode == 'start') {
            $('#timer_btn_stop').hide();

            $('#timer_btn_stop').unbind();
            $('#timer_btn_start').unbind();

            $('#timer_btn_start').show();
            $('#timer_btn_start').click(function(){
                Task.toggle(task_id);
            });
        } else if (mode == 'stop') {
            $('#timer_btn_start').hide();

            $('#timer_btn_start').unbind();
            $('#timer_btn_stop').unbind();

            $('#timer_btn_stop').show();
            $('#timer_btn_stop').click(function(){
                Task.toggle(task_id);
            });
        }
    }
};
