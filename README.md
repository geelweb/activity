#TaskTimer

Track time spent on your projects

[![wercker status](https://app.wercker.com/status/fe553f00453c366c56b2240980cf79e9/s/master "wercker status")](https://app.wercker.com/project/bykey/fe553f00453c366c56b2240980cf79e9)

## Requirements

 * Django >= 1.6

see `requirements.txt` for all dependances

## Contibute

    pip install -r requirements
    python manage.py syncdb
    python manage.py runserver

## Install

    git clone git@bitbucket.org:geelweb/activity.git

Postgres

    $ sudo apt-get install postgresql9.3 python-psycopg2
    $ sudo sed -i.bak -e 's/\(^local.*\)\(peer\)$/\1trust/g' /etc/postgresql/9.3/main/pg_hba.conf
    $ sudo /etc/init.d/postgresql restart

    $ createdb -U postgres dbactivity

Requirements

    $ pip install -r requirements.txt

Statics

    $ sudo mkdir -p /var/www/static
    $ sudo apt-get install lighttpd
    $ sudo vim /etc/lighttpd/lighttpd.conf

Add

    server.port                 = 3000

    $HTTP["host"] =~ "static.activity.geelweb.fr$" {
            server.document-root = "/var/www/static"
    }


Collect statics

    $ cd activity
    $ python manage.py collectstatic


Run the app

    $ sudo pip install gunicorn supervisor
    $ mkdir -p activity/logs
    $ touch activity/logs/gunicorn_supervisor.log
    $ sudo vim /etc/supervisor/conf.d/activity.conf

    [program:activity]
    command=/usr/local/bin/gunicorn -b 0.0.0.0:8000 activity.wsgi:application
    directory=/home/gluchet/activity
    user=ubuntu
    autostart=true
    autorestart=true
    stdout_logfile = /home/gluchet/activity/logs/gunicorn_supervisor.log
    redirect_stderr=true

    $ sudo supervisorctl reread
    $ sudo supervisorctl update
## Features

## Todo

 * Upgrade libs
 * Roadmap
 * Changelog
 * Doc
 * Installer
