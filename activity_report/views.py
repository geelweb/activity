from django.shortcuts import render_to_response
from django.template import RequestContext
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.db.models import Sum

from datetime import date, datetime, timedelta
import json

from activity_core.models import Client, Project, Task

def get_period(data={}):
    """
    >>> get_period()
    (date(date.today().year, date.today().month, 1), date.today())

    >>> get_period({'from_date': '2013-01-01', 'to_date': '2013-01-31'})
    (date(2013, 1, 1), date(2013, 1, 31))
    """
    today = date.today()
    from_date = date(today.year, today.month, 1)
    to_date = date.today()
    if 'to_date' in data:
        to_date = datetime.strptime(data['to_date'], '%Y-%m-%d').date()
    if 'from_date' in data:
        from_date = datetime.strptime(data['from_date'], '%Y-%m-%d').date()
    return (from_date, to_date)

@login_required
def index(request):
    from_date, to_date = get_period(request.GET)
    client = None
    if 'client' in request.GET:
        client = request.GET['client']
    project = None
    if 'project' in request.GET:
        project = request.GET['project']

    return render_to_response(
        'activity_report/index.html', {
            'from_date': from_date.strftime('%Y-%m-%d'),
            'to_date': to_date.strftime('%Y-%m-%d'),
            'client': client,
            'project': project
        }, context_instance=RequestContext(request))

@login_required
def treemap_clients(request):
    from_date, to_date = get_period(request.GET)

    data = {"name": "clients", "children":[]}
    if 'client' in request.GET and request.GET['client'] != 'None':
        clients = Client.objects.filter(company__in=request.user.company_set.all(),
                pk=request.GET['client'])
    else:
        clients = Client.objects.filter(company__in=request.user.company_set.all())
    for client in clients:
        client_item = {"name": client.name, "children": []}
        projects = Project.objects.filter(
                client=client,
                task__job__start__gte="%s 00:00:00" % from_date,
                task__job__stop__lte="%s 23:59:59" % to_date)
        projects = projects.annotate(time_spent=Sum('task__job__duration'))
        for project in projects:
            project_item = {
                "name": "%s (%s)" % (project.name, timedelta(seconds=project.time_spent or 0)),
                "size": project.time_spent,
                "children": [],
            }
            tasks = Task.objects.filter(
                    project=project,
                    job__start__gte="%s 00:00:00" % from_date,
                    job__stop__lte="%s 23:59:59" % to_date)
            tasks = tasks.annotate(time_spent=Sum('job__duration'))
            for task in tasks:
                project_item['children'].append({
                    "name": "%s (%s)" % (task.name, timedelta(seconds=task.time_spent or 0)),
                    "size": task.time_spent,
                })
            client_item['children'].append(project_item)
        data['children'].append(client_item)

    if len(data['children']) == 1:
        data = data['children'][0]
    return HttpResponse(json.dumps(data), content_type='application/json')

