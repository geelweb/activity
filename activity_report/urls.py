from django.conf.urls import patterns, include, url

urlpatterns = patterns('activity_report.views',
    url(r'^$', 'index', name='report'),
    url(r'^treemap-clients/$', 'treemap_clients', name='treemap_clients'),
)
