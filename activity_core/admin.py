from django.contrib import admin
from activity_core.models import *

class ProjectAdmin(admin.ModelAdmin):
    list_display = ('name', 'client')
    list_filter = ['client']

class TaskAdmin(admin.ModelAdmin):
    list_display = ('name', 'project')
    list_filter = ['project__client']

class ClientAdmin(admin.ModelAdmin):
    list_display = ('name', 'company')

class CompanyAdmin(admin.ModelAdmin):
    list_display = ('name', 'owner')

admin.site.register(Company, CompanyAdmin)
admin.site.register(Contact)
admin.site.register(Client, ClientAdmin)
admin.site.register(Project, ProjectAdmin)
admin.site.register(Task, TaskAdmin)
