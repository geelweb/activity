import json
import urllib
import hashlib

from django.views.generic import ListView, DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib.auth.models import User
from django.core.exceptions import PermissionDenied
from django.core.urlresolvers import reverse_lazy
from django.core import serializers
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404
from django.db.models import Sum, Q


from activity_core.models import *
from activity_core.forms import *

def user_is_developer(u):
    # TODO remove me
    return u.is_superuser #or u.groups.filter(name='developer').count() == 1

class AjaxMixinListView(ListView):
    def render_to_response(self, context, **response_kwargs):
        if self.request.is_ajax():
            data = serializers.serialize("json", context['object_list'])
            response_kwargs['content_type'] = 'application/json'
            return HttpResponse(data, **response_kwargs)

        return super(AjaxMixinListView, self).render_to_response(context, **response_kwargs)

class ClientsView(AjaxMixinListView):
    """
    Clients list
    """
    model = Client

    def get_queryset(self):
        queryset = Client.objects.filter(company__in=self.request.user.company_set.all())
        return queryset

class ClientCreateView(CreateView):
    model = Client
    form_class = ClientForm
    success_url = reverse_lazy('clients')

class ClientUpdateView(UpdateView):
    model = Client
    form_class = ClientForm
    success_url = reverse_lazy('clients')

class ClientDeleteView(DeleteView):
    model = Client
    success_url = reverse_lazy('clients')

class ProjectsView(AjaxMixinListView):
    """
    Projects list
    """
    model = Project

    def dispatch(self, *args, **kwargs):
        self.client = get_object_or_404(Client, pk=kwargs['client_id'],
            company__in=self.request.user.company_set.all())
        return super(ProjectsView, self).dispatch(*args, **kwargs)

    def get_queryset(self):
        return Project.objects.annotate(time_spent=Sum('task__job__duration')).filter(client=self.client)

    def get_context_data(self, **kwargs):
        context = super(ProjectsView, self).get_context_data(**kwargs)
        context['client'] = self.client
        return context

class ProjectCreateView(CreateView):
    model = Project
    form_class = ProjectForm

    def dispatch(self, *args, **kwargs):
        self.client = get_object_or_404(Client, pk=kwargs['client_id'],
            company__in=self.request.user.company_set.all())
        return super(ProjectCreateView, self).dispatch(*args, **kwargs)

    def get_success_url(self):
        return reverse_lazy('projects', kwargs={'client_id': self.client.id})

    def form_valid(self, form):
        form.instance.client = self.client
        return super(ProjectCreateView, self).form_valid(form)

class ProjectUpdateView(UpdateView):
    model = Project
    form_class = ProjectForm

    def dispatch(self, *args, **kwargs):
        self.client = get_object_or_404(Client, pk=kwargs['client_id'])
        return super(ProjectUpdateView, self).dispatch(*args, **kwargs)

    def get_success_url(self):
        return reverse_lazy('projects', kwargs={'client_id': self.client.id})

class ProjectDeleteView(DeleteView):
    model = Project

    def dispatch(self, *args, **kwargs):
        self.client = get_object_or_404(Client, pk=kwargs['client_id'])
        return super(ProjectDeleteView, self).dispatch(*args, **kwargs)

    def get_success_url(self):
        return reverse_lazy('projects', kwargs={'client_id': self.client.id})

class TasksView(ListView):
    """
    Tasks list
    """
    model = Task

    def dispatch(self, *args, **kwargs):
        self.client = get_object_or_404(Client, pk=self.kwargs['client_id'],
            company__in=self.request.user.company_set.all())
        self.project = get_object_or_404(Project, pk=self.kwargs['project_id'],
            client=self.client)
        return super(TasksView, self).dispatch(*args, **kwargs)

    def get_queryset(self):
        return Task.objects.annotate(time_spent=Sum('job__duration')).filter(project=self.project)

    def get_context_data(self, **kwargs):
        context = super(TasksView, self).get_context_data(**kwargs)
        context['client'] = self.client
        context['project'] = self.project
        context['can_change_or_view_job'] = user_is_developer(self.request.user)
        context['can_start_stop_job'] = user_is_developer(self.request.user)
        return context

class TaskDetailsView(DetailView):
    model = Task

class TaskCreateView(CreateView):
    model = Task
    form_class = TaskForm

    def dispatch(self, *args, **kwargs):
        self.client = get_object_or_404(Client, pk=self.kwargs['client_id'],
            company__in=self.request.user.company_set.all())
        self.project = get_object_or_404(Project, pk=self.kwargs['project_id'],
            client=self.client)
        return super(TaskCreateView, self).dispatch(*args, **kwargs)

    def get_success_url(self):
        return reverse_lazy('tasks', kwargs={'client_id': self.client.id,
            'project_id': self.project.id})

    def form_valid(self, form):
        form.instance.project = self.project
        return super(TaskCreateView, self).form_valid(form)

class TaskUpdateView(UpdateView):
    model = Task
    form_class = TaskForm

    def dispatch(self, *args, **kwargs):
        self.client = get_object_or_404(Client, pk=self.kwargs['client_id'],
            company__in=self.request.user.company_set.all())
        self.project = get_object_or_404(Project, pk=self.kwargs['project_id'],
            client=self.client)
        return super(TaskUpdateView, self).dispatch(*args, **kwargs)

    def get_success_url(self):
        return reverse_lazy('tasks', kwargs={'client_id': self.client.id,
            'project_id': self.project.id})

class TaskDeleteView(DeleteView):
    model = Task

    def dispatch(self, *args, **kwargs):
        self.client = get_object_or_404(Client, pk=self.kwargs['client_id'],
            company__in=self.request.user.company_set.all())
        self.project = get_object_or_404(Project, pk=self.kwargs['project_id'],
            client=self.client)
        return super(TaskDeleteView, self).dispatch(*args, **kwargs)

    def get_success_url(self):
        return reverse_lazy('tasks', kwargs={'client_id': self.client.id,
            'project_id': self.project.id})

@login_required
def toggle_task(request, task_id):
    """
    Ajax view to start/stop tasks
    """
    try:
        #task = Task.objects.get(pk=task_id)
        task = Task.objects.annotate(time_spent=Sum('job__duration')).get(pk=task_id)
    except Task.DoesNotExist:
        return HttpResponse(json.dumps({'code': 404}), content_type='application/json')

    result = {
        'code': 200,
        'task_id': task_id,
        'time': task.time_spent,
    }

    if task.start(request.user):
        result['status'] = 101
    elif task.stop(request.user):
        result['status'] = 102
    else:
        result['status'] = 500

    return HttpResponse(json.dumps(result), content_type='application/json')

@login_required
#@user_passes_test(user_is_developer)
def last_task(request):
    """
    Ajax view to gets the last-task block content
    """
    if not user_is_developer(request.user):
        raise PermissionDenied


    try:
        job = Job.objects.filter(user=request.user).order_by('-start')[0]
        task = Task.objects.annotate(time_spent=Sum('job__duration')).get(pk=job.task.id)
    except IndexError:
        job = task = False

    return render_to_response('ajax_last_task.html', {
        'job': job,
        'task': task,
        }, context_instance=RequestContext(request))

class ContactsListView(ListView):
    model = Contact

class ContactCreateView(CreateView):
    model = Contact
    success_url = reverse_lazy('contacts')

class ContactUpdateView(UpdateView):
    model = Contact
    success_url = reverse_lazy('contacts')

class CompaniesListView(ListView):
    model = Company

    def get_queryset(self):
        queryset = self.request.user.company_set.all()
        return queryset

class CompanyCreateView(CreateView):
    model = Company
    fields =['name']
    success_url = reverse_lazy('companies')

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.owner = self.request.user
        self.object.save()
        self.object.users.add(self.request.user)
        return HttpResponseRedirect(self.get_success_url())

class CompanyUpdateView(UpdateView):
    model = Company
    fields = ['name']
    success_url = reverse_lazy('companies')
    template_name_suffix = '_update_form'

def ajx_users_list(request):
    collection = []
    if 'q' in request.GET:
        term = request.GET['q']
        users = User.objects.filter(Q(first_name__icontains=term) |
                Q(last_name__icontains=term) | Q(username__icontains=term))
        for user in users:
            size = 40
            gravatar_url = "http://www.gravatar.com/avatar/" + hashlib.md5(user.email.lower()).hexdigest() + "?"
            gravatar_url += urllib.urlencode({'s':str(size)})
            collection.append({
                'id': user.pk,
                'username': user.username,
                'email' : user.email,
                'gravatar_url' : gravatar_url,
                'first_name': user.first_name,
                'last_name': user.last_name,
            })

    return HttpResponse(json.dumps(collection), content_type='application/json')

def select2_list(request, app, model):
    from django.db.models import get_model
    my_model = get_model(app, model)

    collection = []
    if 'q' in request.GET:
        term = request.GET['q']
        contacts = my_model.objects.filter(Q(name__icontains=term))
        for contact in contacts:
            collection.append({
                'id': contact.pk,
                'text': contact.name,
            })
    return HttpResponse(json.dumps(collection), content_type='application/json')

def company_members(request, pk):
    company = get_object_or_404(Company, pk=pk)
    return render_to_response('activity_core/company_members.html', {
        'company': company,
    }, context_instance=RequestContext(request))

def add_company_users(request):
    if request.method == 'POST':
        company = Company.objects.get(pk=request.POST['company'])
        for user_id in request.POST.getlist('users'):
            user = User.objects.get(pk=user_id)
            company.users.add(user)
    return HttpResponseRedirect(reverse_lazy('company-members', kwargs={'pk': company.pk}))

def remove_company_user(request):
    if request.method == 'POST':
        company = Company.objects.get(pk=request.POST['company'])
        user = User.objects.get(pk=request.POST['user'])
        company.users.remove(user)
    return HttpResponseRedirect(reverse_lazy('company-members', kwargs={'pk': company.pk}))
