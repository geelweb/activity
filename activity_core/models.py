from django.db import models
from django.contrib.auth.models import User, Group
from django.utils import timezone

class Contact(models.Model):
    name = models.CharField(max_length=200)
    email = models.EmailField(blank=True, null=True)
    phone = models.CharField(max_length=15, blank=True, null=True)

    def __unicode__(self):
        return self.name

class Company(models.Model):
    name = models.CharField(max_length=200)
    owner = models.ForeignKey(User, related_name='owner')
    users = models.ManyToManyField(User)

    class Meta:
        verbose_name_plural = "companies"

    def __unicode__(self):
        return self.name

class Client(models.Model):
    name = models.CharField(max_length=200)
    company = models.ForeignKey(Company, blank=True, null=True)
    contact = models.ForeignKey(Contact, blank=True, null=True)

    def __unicode__(self):
        return self.name

class Project(models.Model):
    name = models.CharField(max_length=200)
    client = models.ForeignKey(Client)

    def __unicode__(self):
        return self.name

class Task(models.Model):
    name = models.CharField(max_length=200)
    description = models.TextField(blank=True, null=True)
    project = models.ForeignKey(Project)

    def __unicode__(self):
        return self.name

    def startable(self, user):
        """
        Returns true if no jobs are found for the given user with stop not yet
        defined
        """
        jobs = Job.objects.filter(task=self.id, user=user, stop=None)
        return len(jobs) == 0

    def stopable(self, user):
        """
        Returns true if the given user has a job without stop date
        """
        jobs = Job.objects.filter(task=self.id, user=user, stop=None)
        return len(jobs) == 1

    def start(self, user):
        if not self.startable(user):
            return False

        job = Job(task=self, user=user, start=timezone.now())
        job.save()
        return True

    def stop(self, user):
        if not self.stopable(user):
            return False

        job = Job.objects.filter(task=self.id, stop=None)[0]
        job.stop = timezone.now()
        job.duration = int((job.stop - job.start).total_seconds())
        job.save()

        return True

class Job(models.Model):
    task = models.ForeignKey(Task)
    user = models.ForeignKey(User)
    start = models.DateTimeField()
    stop = models.DateTimeField(blank=True, null=True)
    duration = models.IntegerField(blank=True, null=True)

    def get_time_spent(self):
        if self.stop == None:
            return int((timezone.now() - self.start).total_seconds())
        if self.duration != None:
            return self.duration
        return 0

