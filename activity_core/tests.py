from django.test import TestCase
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User
from activity_core.models import *

class ClientCrudTest(TestCase):
    fixtures = ['user-data.json', 'client-data.json']

    def setUp(self):
        self.client.login(username='root', password='secret')

    def test_list(self):
        resp = self.client.get(reverse('clients'))
        self.assertEqual(resp.status_code, 200)

    def test_create(self):
        resp = self.client.get(reverse('client-create'))
        self.assertEqual(resp.status_code, 200)

        resp = self.client.post(reverse('client-create'), {
            'name':'My Client Name'}, follow=True)
        self.assertEqual(resp.redirect_chain, [('http://testserver' + reverse('clients'), 302)])

        client = Client.objects.get(pk=3)
        self.assertEqual(client.name, 'My Client Name')

    def test_update(self):
        resp = self.client.get(reverse('client-update', kwargs={'pk': 1}))
        self.assertEqual(resp.status_code, 200)

        resp = self.client.post(reverse('client-update', kwargs={'pk': 1}), {
            'name':'My Updated Name'}, follow=True)
        self.assertEqual(resp.redirect_chain, [('http://testserver' + reverse('clients'), 302)])

        client = Client.objects.get(pk=1)
        self.assertEqual(client.name, 'My Updated Name')

    def test_delete(self):
        resp = self.client.get(reverse('client-delete', kwargs={'pk': 2}))
        self.assertEqual(resp.status_code, 200)

        resp = self.client.post(reverse('client-delete', kwargs={'pk': 2}), {}, follow=True)
        self.assertEqual(resp.redirect_chain, [('http://testserver' + reverse('clients'), 302)])

        with self.assertRaises(Client.DoesNotExist):
            Client.objects.get(pk=2)

class ProjectCrudTest(TestCase):
    fixtures = ['user-data.json', 'client-data.json', 'project-data.json']

    def setUp(self):
        self.client.login(username='root', password='secret')

    def test_list(self):
        resp = self.client.get(reverse('projects', kwargs={'client_id': 1}))
        self.assertEqual(resp.status_code, 200)

    def test_create(self):
        resp = self.client.get(reverse('project-create', kwargs={'client_id': 1}))
        self.assertEqual(resp.status_code, 200)

        resp = self.client.post(reverse('project-create', kwargs={'client_id': 1}), {
            'name':'My Project Name'}, follow=True)
        self.assertEqual(resp.redirect_chain, [('http://testserver' + reverse('projects', kwargs={'client_id': 1}), 302)])

        project = Project.objects.get(pk=3)
        self.assertEqual(project.name, 'My Project Name')

    def test_update(self):
        resp = self.client.get(reverse('project-update', kwargs={'client_id': 1, 'pk': 1}))
        self.assertEqual(resp.status_code, 200)

        resp = self.client.post(reverse('project-update', kwargs={'client_id': 1, 'pk': 1}), {
            'name':'My Updated Project Name'}, follow=True)
        self.assertEqual(resp.redirect_chain, [('http://testserver' + reverse('projects', kwargs={'client_id': 1}), 302)])

        project = Project.objects.get(pk=1)
        self.assertEqual(project.name, 'My Updated Project Name')

    def test_delete(self):
        resp = self.client.get(reverse('project-delete', kwargs={'client_id': 1, 'pk': 2}))
        self.assertEqual(resp.status_code, 200)

        resp = self.client.post(reverse('project-delete', kwargs={'client_id': 1, 'pk': 2}), {}, follow=True)
        self.assertEqual(resp.redirect_chain, [('http://testserver' + reverse('projects', kwargs={'client_id': 1}), 302)])

        with self.assertRaises(Project.DoesNotExist):
            Project.objects.get(pk=2)

class TaskCrudTest(TestCase):
    fixtures = ['user-data.json', 'client-data.json', 'project-data.json', 'task-data.json']

    def setUp(self):
        self.client.login(username='root', password='secret')

    def test_list(self):
        resp = self.client.get(reverse('tasks', kwargs={'client_id': 1, 'project_id': 1}))
        self.assertEqual(resp.status_code, 200)

    def test_create(self):
        resp = self.client.get(reverse('task-create', kwargs={'client_id': 1, 'project_id': 1}))
        self.assertEqual(resp.status_code, 200)

        resp = self.client.post(reverse('task-create', kwargs={'client_id': 1, 'project_id': 1}), {
            'name':'My Task Name'}, follow=True)
        self.assertEqual(resp.redirect_chain, [('http://testserver' + reverse('tasks', kwargs={'client_id': 1, 'project_id': 1}), 302)])

        task = Task.objects.get(pk=3)
        self.assertEqual(task.name, 'My Task Name')

    def test_update(self):
        resp = self.client.get(reverse('task-update', kwargs={'client_id': 1, 'project_id': 1, 'pk': 1}))
        self.assertEqual(resp.status_code, 200)

        resp = self.client.post(reverse('task-update', kwargs={'client_id': 1, 'project_id': 1, 'pk': 1}), {
            'name':'My Updated Task Name'}, follow=True)
        self.assertEqual(resp.redirect_chain, [('http://testserver' + reverse('tasks', kwargs={'client_id': 1, 'project_id': 1}), 302)])

        task = Task.objects.get(pk=1)
        self.assertEqual(task.name, 'My Updated Task Name')

    def test_delete(self):
        resp = self.client.get(reverse('task-delete', kwargs={'client_id': 1, 'project_id': 1, 'pk': 2}))
        self.assertEqual(resp.status_code, 200)

        resp = self.client.post(reverse('task-delete', kwargs={'client_id': 1, 'project_id': 1, 'pk': 2}), {}, follow=True)
        self.assertEqual(resp.redirect_chain, [('http://testserver' + reverse('tasks', kwargs={'client_id': 1, 'project_id': 1}), 302)])

        with self.assertRaises(Task.DoesNotExist):
            Task.objects.get(pk=2)

class CompanyCrudTest(TestCase):
    fixtures = ['user-data.json']

    def setUp(self):
        self.client.login(username='root', password='secret')

    def test_list(self):
        resp = self.client.get(reverse('companies'))
        self.assertEqual(resp.status_code, 200)

    def test_create(self):
        resp = self.client.get(reverse('company-create'))
        self.assertEqual(resp.status_code, 200)

        resp = self.client.post(reverse('company-create'), {
            'name': 'A company name'}, follow=True)
        self.assertEqual(resp.redirect_chain, [('http://testserver' + reverse('companies'), 302)])

        company = Company.objects.filter(name='A company name')[0]
        self.assertEqual(company.owner, resp.context['user'])
        self.assertIn(resp.context['user'], company.users.all())
