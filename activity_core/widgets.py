from itertools import chain

from django import forms
from django.utils.safestring import mark_safe

class Select2Widget(forms.Select):
    options = {
        'ajax': False,
    }

    def __init__(self, **kwargs):
        select2_options = kwargs.pop('select2_options', {})
        for name, value in select2_options.items():
            self.options[name] = value

        super(Select2Widget, self).__init__(**kwargs)

    def render_js(self, id=None, choices=()):
        if not self.options['ajax']:
            js = """
            <script type="text/javascript">
                $('#%s').select2();
            </script>
            """ % id
            return js

        js = """
        <script type="text/javascript">
            $('#%s').select2({
                ajax: {
                    url: '/select2/%s/%s/',
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            q: params.term,
                            page: params.page
                        };
                    },
                    processResults: function (data, page) {
                        return {
                            results: data
                        };
                    },
                    cache: true
                }
            });
        </script>
        """ % (id, self.options['app'], self.options['model'])
        return js

    def render(self, name, value, attrs=None, choices=()):
        attrs['class'] = '%s select2-ajax' % attrs['class']
        html = super(Select2Widget, self).render(name, value, attrs, choices)
        html += self.render_js(attrs['id'], choices)
        return mark_safe(html)

