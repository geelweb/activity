from django.forms import ModelForm
from django.contrib.auth.models import User, Group

from activity_core.models import Task, Project, Client
from activity_core.widgets import Select2Widget

class TaskForm(ModelForm):
    class Meta:
        model = Task
        fields = ['name', 'description']

class ClientForm(ModelForm):
    class Meta:
        model = Client
        fields = ['name', 'contact']
        widgets = {
            'contact': Select2Widget(select2_options={
                'ajax': True,
                'app': 'activity_core',
                'model': 'contact',
            })
        }

class ProjectForm(ModelForm):
    class Meta:
        model = Project
        fields = ['name']
