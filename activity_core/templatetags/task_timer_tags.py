from django import template
from datetime import timedelta

register = template.Library()

@register.filter
def startable(obj, user):
    return obj.startable(user)

@register.filter
def duration(seconds):
    if seconds is None:
        seconds = 0

    return timedelta(seconds=seconds)
