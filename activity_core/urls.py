from django.conf.urls import patterns, include, url
from django.contrib.auth.decorators import login_required

from activity_core.views import *

urlpatterns = patterns('',
    url(r'^contacts/$', login_required(ContactsListView.as_view()), name='contacts'),
    url(r'^contacts/add/$', login_required(ContactCreateView.as_view()), name='contact-create'),
    url(r'^contacts/(?P<pk>\d+)$', login_required(ContactUpdateView.as_view()), name='contact-update'),

    url(r'^companies/$', login_required(CompaniesListView.as_view()), name='companies'),
    url(r'^companies/add/$', login_required(CompanyCreateView.as_view()), name='company-create'),
    url(r'^companies/(?P<pk>\d+)/$', login_required(CompanyUpdateView.as_view()), name='company-update'),
    url(r'^companies/add-user/$', 'activity_core.views.add_company_users'),
    url(r'^companies/remove-user/$', 'activity_core.views.remove_company_user'),
    url(r'^companies/(?P<pk>\d+)/members/$', 'activity_core.views.company_members', name='company-members'),

    # Clients CRUD urls
    url(r'^clients/$', login_required(ClientsView.as_view()), name='clients'),
    url(r'^clients/add/$', login_required(ClientCreateView.as_view()), name='client-create'),
    url(r'^clients/(?P<pk>\d+)$', login_required(ClientUpdateView.as_view()), name='client-update'),
    url(r'^clients/(?P<pk>\d+)/delete/$', login_required(ClientDeleteView.as_view()), name='client-delete'),

    # Projects CRUD urls
    url(r'^clients/(?P<client_id>\d+)/projects/$', login_required(ProjectsView.as_view()), name='projects'),
    url(r'^clients/(?P<client_id>\d+)/projects/add/$', login_required(ProjectCreateView.as_view()), name='project-create'),
    url(r'^clients/(?P<client_id>\d+)/projects/(?P<pk>\d+)/$', login_required(ProjectUpdateView.as_view()), name='project-update'),
    url(r'^clients/(?P<client_id>\d+)/projects/(?P<pk>\d+)/delete/$', login_required(ProjectDeleteView.as_view()), name='project-delete'),

    # Tasks CRUD urls
    url(r'^clients/(?P<client_id>\d+)/projects/(?P<project_id>\d+)/tasks/$', login_required(TasksView.as_view()), name='tasks'),
    url(r'^clients/(?P<client_id>\d+)/projects/(?P<project_id>\d+)/tasks/add/$', login_required(TaskCreateView.as_view()), name='task-create'),
    url(r'^clients/(?P<client_id>\d+)/projects/(?P<project_id>\d+)/tasks/(?P<pk>\d+)/$', login_required(TaskUpdateView.as_view()), name='task-update'),
    url(r'^clients/(?P<client_id>\d+)/projects/(?P<project_id>\d+)/tasks/(?P<pk>\d+)/delete/$', login_required(TaskDeleteView.as_view()), name='task-delete'),

    # Ajax views to manage tasks
    url(r'^ajx/task/(?P<task_id>\d+)/toggle/$', 'activity_core.views.toggle_task'),
    url(r'^ajx/task/last/$', 'activity_core.views.last_task', name='last-task'),
    url(r'^ajx/users/$', 'activity_core.views.ajx_users_list'),

    url(r'^select2/(?P<app>\w+)/(?P<model>\w+)/$', 'activity_core.views.select2_list'),
)


