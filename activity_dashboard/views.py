from datetime import datetime

from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.shortcuts import render_to_response
from django.template import RequestContext

from django.db.models import Sum

from activity_core.models import *

@login_required
def dashboard(request):
    return render_to_response('activity_dashboard/dashboard.html', {
        }, context_instance=RequestContext(request))

def widget_hours_month(request):
    start = datetime(datetime.now().year, datetime.now().month, 1)

    projects = Project.objects.filter(
            client__company__in=request.user.company_set.all(),
            task__job__start__gte=start)
    data = projects.annotate(time_spent=Sum('task__job__duration')).aggregate(Sum('time_spent'))
    return render_to_response('activity_dashboard/widgets/hours_month.html', {
            'h': '%.2f' % (data['time_spent__sum'] / 60.0 / 60.0),
        }, context_instance=RequestContext(request))

def widget_projects_month(request):
    start = datetime(datetime.now().year, datetime.now().month, 1)

    projects = Project.objects.filter(
            client__company__in=request.user.company_set.all(),
            task__job__start__gte=start).distinct().count()
    return render_to_response('activity_dashboard/widgets/projects_month.html', {
            'n': projects,
        }, context_instance=RequestContext(request))

def widget_contributors_month(request):
    start = datetime(datetime.now().year, datetime.now().month, 1)

    jobs = Job.objects.filter(
            task__project__client__company__in=request.user.company_set.all(),
            start__gte=start).values('user').distinct().count()
    return render_to_response('activity_dashboard/widgets/contributors_month.html', {
            'n': jobs,
        }, context_instance=RequestContext(request))

def widget_clients_repartition_month(request):
    start = datetime(datetime.now().year, datetime.now().month, 1)

    clients = Client.objects.filter(
            company__in=request.user.company_set.all(),
            project__task__job__start__gte=start)
    clients = clients.annotate(time_spent=Sum('project__task__job__duration'))
    return render_to_response('activity_dashboard/widgets/clients_repartition_month.html', {
            'clients': clients,
        }, context_instance=RequestContext(request))
