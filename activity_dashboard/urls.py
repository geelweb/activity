from django.conf.urls import patterns, url

urlpatterns = patterns('activity_dashboard.views',
    url(r'^$', 'dashboard', name='dashboard'),

    # widget
    url(r'^widget/hours_month', 'widget_hours_month'),
    url(r'^widget/projects_month', 'widget_projects_month'),
    url(r'^widget/contributors_month', 'widget_contributors_month'),
    url(r'^widget/clients_repartition_month', 'widget_clients_repartition_month'),
)
